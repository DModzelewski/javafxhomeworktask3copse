package workers;

import javafx.concurrent.Task;
import main.Ent;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Watchable;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OpenWorker extends Task<Void> {

    private static final Logger log = Logger.getLogger(SaveWorker.class.getCanonicalName());
    private Collection<Ent> ents;
    private File file;

    public OpenWorker(Collection<Ent> ents, File file) {
        this.ents = ents;
        this.file = file;
    }

    @Override
    protected Void call() throws Exception {
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))){
            int size = (Integer)ois.readObject();
            for (int i=0; i<size; i++){
                ents.add((Ent)ois.readObject());
                updateProgress(i+1,size);
            }
        }catch (Exception e){
            log.log(Level.WARNING, e.getMessage(),e);
        }finally {
            Thread.sleep(200);
            updateProgress(0,1);
        }
        return null;
    }
}
