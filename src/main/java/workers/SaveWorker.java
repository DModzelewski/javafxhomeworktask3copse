package workers;

import javafx.concurrent.Task;
import main.Ent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SaveWorker extends Task<Void> {

    private static final Logger log = Logger.getLogger(SaveWorker.class.getCanonicalName());
    private Collection<Ent> ents;
    private File file;

    public SaveWorker(Collection<Ent> ents, File file) {
        this.ents = ents;
        this.file = file;
    }
    @Override
    protected Void call() throws Exception {
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(ents.size());
            int i=0;
            for(Ent e: ents){
                oos.writeObject(e);
                updateProgress(i+1,ents.size());
                i++;
            }

        }catch (Exception e){
            log.log(Level.WARNING, e.getMessage(), e);
        }finally {
            Thread.sleep(200);
            updateProgress(0,1);
        }
        return null;
    }
}
