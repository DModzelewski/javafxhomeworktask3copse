package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import java.net.URL;
import java.util.ResourceBundle;

public class CopseMain extends Application{
    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        URL fxml = this.getClass().getResource("/ents_list.fxml");
        ResourceBundle rb = ResourceBundle.getBundle("ents_list_msg");

        Parent root = FXMLLoader.load(fxml,rb);
        Scene scene = new Scene(root,600,500, Color.WHITE);
        scene.getStylesheets().add("/ents_list.css");
        stage.setScene(scene);
        stage.show();
    }
}
