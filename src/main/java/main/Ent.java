package main;

import java.io.Serializable;

public class Ent implements Serializable {
    private int plantYear;
    private int height;
    private String species;
    private Type type;

    public Ent(int plantYear, int height, String species, Type type) {
        this.plantYear = plantYear;
        this.height = height;

        this.species = species;
        this.type = type;
    }

    public enum Type{
        CONIFEROUS,
        DECIDUOUS,
        unspecified, //default for new ent
    }

    public int getPlantYear() {
        return plantYear;
    }

    public void setPlantYear(int plantYear) {
        this.plantYear = plantYear;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
