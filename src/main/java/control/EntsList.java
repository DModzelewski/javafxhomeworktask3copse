package control;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ChoiceBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.util.converter.IntegerStringConverter;
import main.Ent;
import workers.*;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class EntsList implements Initializable {

    public static final String[] treeSpecies = {"Willow","Oak","Pine","Birch","Cactus","Spruce",
            "Maple","Yew"};

    private ObservableList<Ent> ents = FXCollections.observableArrayList();
    private ResourceBundle rb;

    @FXML
    private ProgressBar progressBar;

    @FXML
    TableView entsTable;

    @FXML
    TableColumn<Ent,Integer> year;
    @FXML
    TableColumn<Ent,Integer> height;
    @FXML
    TableColumn<Ent,String> species;
    @FXML
    TableColumn<Ent,Ent.Type> type;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        rb = resourceBundle; //for events
        progressBar.setProgress(0); //for events updating progress

        entsTable.setItems(ents);
        ents.add(new Ent(1900,2070,"Willow", Ent.Type.DECIDUOUS));
        ents.add(new Ent(1742,4310,"Oak", Ent.Type.DECIDUOUS));
        ents.add(new Ent(1839,4402,"Pine", Ent.Type.CONIFEROUS));
        ents.add(new Ent(1939,1208,"Birch", Ent.Type.DECIDUOUS));
        ents.add(new Ent(1711,3721,"Spruce", Ent.Type.CONIFEROUS));
        ents.add(new Ent(2004,21,"Cactus", Ent.Type.CONIFEROUS));

        year.setCellFactory(TextFieldTableCell.forTableColumn((new IntegerStringConverter())));
        year.setOnEditCommit(event->event.getRowValue().setPlantYear(event.getNewValue()));

        height.setCellFactory(TextFieldTableCell.forTableColumn((new IntegerStringConverter())));
        height.setOnEditCommit(event->event.getRowValue().setHeight(event.getNewValue()));

        type.setCellFactory(ChoiceBoxTableCell.forTableColumn(Ent.Type.values()));
        type.setOnEditCommit(event->event.getRowValue().setType(event.getNewValue()));

        species.setCellFactory(ChoiceBoxTableCell.forTableColumn(treeSpecies));
        species.setOnEditCommit(event->event.getRowValue().setSpecies(event.getNewValue()));

    }

    @FXML
    private void about(ActionEvent event){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(rb.getString("aboutTitle"));
        alert.setHeaderText(rb.getString("aboutHeader"));
        alert.setContentText(rb.getString("aboutContent"));
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.showAndWait();
    }

    @FXML
    private void save(ActionEvent event){
        FileChooser fc = new FileChooser();
        File file = fc.showSaveDialog(null);
        if (file!=null){
            SaveWorker sw = new SaveWorker(ents, file);
            progressBar.progressProperty().bind(sw.progressProperty());
            new Thread(sw).start();
        }
    }

    @FXML
    private void open(ActionEvent event){
        FileChooser fc = new FileChooser();
        File file = fc.showOpenDialog(null);
        if (file!=null){
            OpenWorker ow = new OpenWorker(ents, file);
            progressBar.progressProperty().bind(ow.progressProperty());
            new Thread(ow).start();
        }
    }

    @FXML
    private void plant(ActionEvent event){
        ents.add(new Ent(2018,0,"",Ent.Type.unspecified));
    }

    @FXML
    private void cut(ActionEvent event){
        if (entsTable.getSelectionModel().getSelectedIndex() >= 0) {
            ents.remove(entsTable.getSelectionModel().getSelectedIndex());
            entsTable.getSelectionModel().clearSelection();
        }
    }

    @FXML
    private void root(ActionEvent event){
        if (entsTable.getSelectionModel().getSelectedIndex()>=0) {
            Ent parentEnt = (Ent)entsTable.getSelectionModel().getSelectedItem();
            ents.add(new Ent(2018,10,parentEnt.getSpecies(),parentEnt.getType()));
        }
    }
}
